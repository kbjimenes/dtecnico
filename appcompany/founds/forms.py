from django.contrib.auth.hashers import make_password
from django import forms
from . import models
class LoginForm(forms.Form):
    email = forms.CharField(
        label="Email",
        widget=forms.EmailInput(attrs={'class': 'form-control'})
        )
    password = forms.CharField(
        label="Password", 
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
        )

class LenderRegisterForm(forms.Form):
    first_name = forms.CharField(
        label="First name",
        widget=forms.TextInput(attrs={'class': 'form-control'})
        )
    last_name = forms.CharField(
        label="Last name",
        widget=forms.TextInput(attrs={'class': 'form-control'})
        )
    email = forms.CharField(
        label="Email", 
        widget=forms.EmailInput(attrs={'class': 'form-control'})
        )

    password = forms.CharField(
        label="Password", 
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
        )
    money = forms.IntegerField(
        label="Money", 
        widget=forms.NumberInput(attrs={'class': 'form-control'})
        )

    def save(self, commit=True):
        cleaned_data = super(LenderRegisterForm, self).clean()
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        email = cleaned_data.get('email')
        password = make_password(cleaned_data.get('password'))
        money = cleaned_data.get('money')

        if commit:
            cuser = models.CustomUser.objects.create(email=email, first_name=first_name, 
                last_name=last_name, password=password)
            cuser.save()
            lender = models.Lender.objects.create(user=cuser, money=money)
            lender.save()
            return cuser, lender

    class Meta: 
        model = models.Lender
        fields = ["__all__"]



class BorrowerRegisterForm(forms.Form):
    bfirst_name = forms.CharField(
        label="First name",
        widget=forms.TextInput(attrs={'class': 'form-control'})
        )
    blast_name = forms.CharField(
        label="Last name",
        widget=forms.TextInput(attrs={'class': 'form-control'})
        )
    bemail = forms.CharField(
        label="Email", 
        widget=forms.EmailInput(attrs={'class': 'form-control'})
        )

    bpassword = forms.CharField(
        label="Password", 
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
        )
    bneed_money_for = forms.CharField(
        label="Need money for",
        widget=forms.TextInput(attrs={'class': 'form-control'})
        )
    bdescription = forms.CharField(
        label="Description",
        widget=forms.Textarea(attrs={'placeholder': '', 
        'class': 'form-control', 'rows' : '3'})
        )
    bamount_needed = forms.IntegerField(
        label="Amount needed", 
        widget=forms.NumberInput(attrs={'class': 'form-control'})
        )

    def save(self, commit=True):
        cleaned_data = super(BorrowerRegisterForm, self).clean()
        first_name = cleaned_data.get('bfirst_name')
        last_name = cleaned_data.get('blast_name')
        email = cleaned_data.get('bemail')
        password = make_password(cleaned_data.get('bpassword'))
        need_money_for = cleaned_data.get('bneed_money_for')
        description = cleaned_data.get('bdescription')
        amount_needed = cleaned_data.get('bamount_needed')

        if commit:
            cuser = models.CustomUser.objects.create(
            	email=email, 
            	first_name=first_name, 
                last_name=last_name, 
                password=password)
            cuser.save()
            borrower = models.Borrower.objects.create(
            	user=cuser, 
            	need_money_for=need_money_for,
            	description=description, 
            	amount_needed=amount_needed)
            borrower.save()
            return cuser, borrower

    class Meta: 
        model = models.Borrower
        fields = ["__all__"]

