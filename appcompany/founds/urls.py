from django.urls import path
from . import views as found_views

app_name = 'founds' 


urlpatterns = [
    path('login/',  found_views.user_login, name='user_login'),
    path('logout/',  found_views.user_logout, name='user_logout'),
    path('register', found_views.register, name='register'),
    path('borrower/<int:u_id>/', found_views.borrower, name='borrower'),
    path('lender/<int:u_id>/', found_views.lender, name='lender'),
   # path('loan/', found_views.loan, name='loan')
 ]


