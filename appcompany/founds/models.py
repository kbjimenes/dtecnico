from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser, BaseUserManager
#from django.contrib.auth.base_user import BaseUserManager
from django.db import models

# Create your models here.

class CustomUserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def get_by_natural_key(self, username):
        case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})

    def create_superuser(self, email, first_name, last_name, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractUser):
    username = models.CharField(max_length=30, unique=True, null=True)
    email = models.EmailField(
        verbose_name='email', 
        max_length=255,
        unique=True
        )
    date_joined = models.DateTimeField(
        verbose_name="date joined", 
        auto_now_add=True)
    last_login = models.DateTimeField(
        verbose_name="last login", 
        auto_now_add=True)
    is_admin = models.BooleanField(
        default=False)
    is_active = models.BooleanField(
        default=True)
    first_name  = models.CharField(
        max_length=40, 
        null=True)
    last_name  = models.CharField(
        max_length=40, 
        null=True)


    objects = CustomUserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ['first_name', 'last_name']

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.get_full_name()

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    class Meta:
        verbose_name =('user')
        verbose_name_plural = ('users')
        swappable = 'AUTH_USER_MODEL'

        
class Lender(models.Model):
    user = models.OneToOneField(CustomUser, 
        on_delete=models.CASCADE, 
        null=True
        )
    money = models.IntegerField(
        default=0
        )

class Borrower(models.Model):
    user = models.OneToOneField(
        CustomUser, 
        on_delete=models.CASCADE, 
        null=True
        )
    need_money_for = models.CharField(
        max_length=300,  
        blank=True
        )
    description = models.CharField(
        max_length=600, blank=True)
    amount_needed = models.IntegerField(
        default=0
        )
    amount_raised = models.IntegerField(
        default=0
        )

class Loan(models.Model):
    lender_user = models.ForeignKey(
        Lender,  
        blank=True, null=True, 
        on_delete=models.CASCADE)
    borrower_user = models.ForeignKey(
        Borrower,  
        blank=True, null=True, 
        on_delete=models.CASCADE)
    ammount = models.IntegerField(default=0)
    lend_date = models.DateTimeField(
        verbose_name="lend date", 
        auto_now_add=True)