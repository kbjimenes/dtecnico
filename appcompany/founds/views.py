from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.db.models import F, Q
from django.db.models import Sum

from . import forms
from . import models

# Create your views here.
def user_login(request):
    form= forms.LoginForm()
    if request.method == "POST":
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            email = request.POST.get('email', None)
            password = request.POST.get('password', None)
            try:
                get_user_name = models.CustomUser.objects.get(email=email)
                user =authenticate(username=get_user_name,password=password)
                if user is not None:
                    login(request, user)
                    if models.Lender.objects.filter(user=user):
                         messages.success(request, f"Welcome to your lender account")
                         return redirect('founds:lender', u_id=user.id)
                    messages.success(request, f"Welcome to your borrower account")
                    return redirect('founds:borrower', u_id=user.id)
                else:
                    messages.error(request, 'Invalid Credentials')
                    return redirect('founds:user_login')
            except Exception as e:
                print(e)
                messages.error(request, 'Wrong Email')
                return redirect('founds:user_login')

    context={'form' : form }
    return render(request, 'login.html', context)

def user_logout(request):
    logout(request)
    return redirect('founds:user_login')

def register(request):
    lender_form= forms.LenderRegisterForm()
    borrower_form= forms.BorrowerRegisterForm()
    if request.method == "POST":
        email = request.POST.get('email', None)
        if(models.CustomUser.objects.filter(email=email)):
            get_user_name = models.CustomUser.objects.get(email=email)
            if get_user_name != None:
                messages.error(request, 'There is another account with this email')
            return redirect('founds:register')            
        if request.POST.get("form_type") == 'lender_form':
            lender_form = forms.LenderRegisterForm(request.POST)
            if lender_form.is_valid():
                    lender_form.save()
                    messages.success(request, f"Your lender account has been created successfully")
                    return redirect('founds:user_login')
                    
        if request.POST.get("form_type") == 'borrower_form':
            borrower_form = forms.BorrowerRegisterForm(request.POST)
            if borrower_form.is_valid():
                    borrower_form.save()
                    messages.success(request, f"Your borrower account has been created successfully")
                    return redirect('founds:user_login')
    context={'lender_form' : lender_form, 'borrower_form': borrower_form}
    return render(request, 'register.html', context)

@login_required
def lender(request, u_id):
    form = forms.LoginForm()
    user = models.CustomUser.objects.get(id = u_id)

    #Check the loan
    if request.method == "POST":
        _lender = request.POST['user_lender']
       # _lender_user = models.CustomUser.objects.get(pk=_lender)
        user_lender=models.Lender.objects.get(pk=_lender)#(user=_lender_user)
        _borrower = request.POST['user_borrower']
     #   _borrower_user = models.CustomUser.objects.get(pk=_borrower)
        user_borrower=models.Borrower.objects.get(pk=_borrower)#(user=_borrower_user)
        ammount = request.POST['ammount']
        if(user_lender and user_borrower and ammount):

            models.Loan.objects.create(
                lender_user=user_lender,
                borrower_user=user_borrower,
                ammount=ammount
                )
            current_raised_ammount = user_borrower.amount_raised
            user_borrower.amount_raised= float(current_raised_ammount)+float(ammount)
            user_borrower.save()

            messages.success(request, f"You have lend money")
            
    # People who need help
    lst_borrowers = models.Borrower.objects.filter(~Q(amount_needed =F('amount_raised')))

    #lst_lender_loans = models.Loan.objects.filter(lender_user=user.lender.id,)
    #Aggregation
    lst_lender_loans = models.Loan.objects.filter(lender_user=user.lender.id).values('borrower_user').annotate(amount_lend=Sum('ammount'))
    for loan in lst_lender_loans:
        loan['borrower_user']  =models.Borrower.objects.get(pk=int(loan['borrower_user']))

    context={ 
    "user" : user, 
    "lst_borrowers" : lst_borrowers, 
    "lst_lender_loans" : lst_lender_loans,
    "form" : form}
    return render(request, 'lender.html', context)


#def loan(request):


@login_required
def borrower(request, u_id):
    user = models.CustomUser.objects.get(id = u_id)
    lst_lender_loans = models.Loan.objects.filter(borrower_user=user.borrower.id).values('lender_user').annotate(amount_lend=Sum('ammount'))
    for loan in lst_lender_loans:
        loan['lender_user']  =models.Borrower.objects.get(pk=int(loan['lender_user']))
    context={"user" : user, "lst_lender_loans" : lst_lender_loans }
    return render(request, 'borrower.html', context)